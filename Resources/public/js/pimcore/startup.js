pimcore.object.tags.wysiwyg.defaultEditorConfig = {
    inline: false,
};

(function(startWysiwygEditor) {
    pimcore.object.tags.wysiwyg.prototype.startWysiwygEditor = function() {
        if(this.ddWysiwyg) {
            return;
        }

        const initializeWysiwyg = new CustomEvent(pimcore.events.initializeWysiwyg, {
            detail: {
                config: this.fieldConfig,
                context: "object"
            },
            cancelable: true
        });
        const initIsAllowed = document.dispatchEvent(initializeWysiwyg);
        if(!initIsAllowed) {
            return;
        }

        const createWysiwyg = new CustomEvent(pimcore.events.createWysiwyg, {
            detail: {
                textarea: this.editableDivId,
                context: "object",
            },
            cancelable: true
        });
        const createIsAllowed = document.dispatchEvent(createWysiwyg);
        if(!createIsAllowed) {
            return;
        }

        document.addEventListener(pimcore.events.changeWysiwyg, function (e) {
            if (this.editableDivId === e.detail.e.target.id) {
                if (pimcore.object.tags.wysiwyg.defaultEditorConfig.inline === false) {
                    this.setValue(Ext.get(this.editableDivId + '_ifr').dom.contentWindow.document.body.innerHTML);
                } else {
                    this.setValue(e.detail.data);
                }

            }
        }.bind(this));

        if (!parent.pimcore.wysiwyg.editors.length) {
            Ext.get(this.editableDivId).dom.addEventListener("keyup", (e) => {
                this.setValue(Ext.get(this.editableDivId).dom.innerText);
            });
        }

        // add drop zone, use the parent panel here (container), otherwise this can cause problems when specifying a fixed height on the wysiwyg
        this.ddWysiwyg = new Ext.dd.DropZone(Ext.get(this.editableDivId).parent(), {
            ddGroup: "element",

            getTargetFromEvent: function(e) {
                return this.getEl();
            },

            onNodeOver : function(target, dd, e, data) {
                if (data.records.length === 1 && this.dndAllowed(data.records[0].data)) {
                    return Ext.dd.DropZone.prototype.dropAllowed;
                }
            }.bind(this),

            onNodeDrop : this.onNodeDrop.bind(this)
        });
    };
}(pimcore.object.tags.wysiwyg.prototype.startWysiwygEditor));