#### Install

run composer require scop/customtinycmebundle:dev-master

To allow bundle, config/bundles.php has to be extended:
```
return [
    ...
    Scop\CustomTinyCMEBundle\ScopCustomTinyCMEBundle::class => ['all' => true],
]
```

run `php bin/console assets:install` to install assets

#### Usage
No extra work needs to be done.
After installation, the TinyCME editors are no longer used in inline mode.