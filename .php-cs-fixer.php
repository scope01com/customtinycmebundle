<?php

$header = <<<EOF
Implemented by scope01 GmbH team https://scope01.com

@copyright scope01 GmbH https://scope01.com
@license proprietär
@link https://scope01.com
EOF;


$config = new PhpCsFixer\Config();

return $config->setRules([
    '@Symfony' => true,
    'array_syntax' => ['syntax' => 'short'],
    'header_comment' => ['header' => $header, 'separate' => 'bottom', 'comment_type' => 'PHPDoc'],
    'full_opening_tag' => false,
]);